# CICD w/ Terraform Workshop

## Summary

It is common in 2020 to manage a workload's software and infrastructure using version controlled programming languages, allowing teams to document their infrastructure as code in the same way they do their application source code. The software-defined nature of cloud computing environments, the large number of APIs and actions that can be scripted, allows teams to not only document their architecture, infrastructure, and application code as source code but also to lift their operational procedures for governing the application in operation out of human-readable documents and put those procedures and responses into scripted controls that enable the system to self-manage. In this way a system scaling up to absorb additional traffic, or scaling down to reduce costs in times of low utilization can now be captured as automated responses. The system's ability to restart failed nodes or to create a replica of an environment as part of a Blue / Green version release or canary deployment, all can be captured and documented as source code.

This collection of labs endeavours to provide you with an example of this process and some of the tools involved. Broadly speaking a version control system, build system, service provider, and programming languages are all that is required. There are a multitude of technologies available to support you:

Version control systems are predominantly Git-based today and are commercially available from GitHub, GitLab, BitBucket, AWS CodeCommit, and others. Build systems include Jenkins, Circle CI, Travis, GitLab Runner, AWS CodePipeline, and many others. Service providers include Amazon Web Services, DataDog, Dome9, CloudHealth, and Salesforce to name a few. Programming languages for software are too numerous to count but for infrastructure and scripting the service providers common technologies include Ansible, Puppet, Terraform, Troposphere, AWS CloudFormation, and Amazon Cloud Developer Kit.

In this collection of labs you will use GitLab for version control, GitLab Runners for a build system, HashiCorp Terraform and RedHat Ansible for infrastructure as code, and Amazon Web Services as the service provider; all to build a CICD pipeline for deploying Wordpress onto AWS using recommended practices for high availability.

## Learning Objectives

The key focus areas for this workshop are on the use of a Git repository (GitLab) and a CICD pipeline (GitLab Runner) to build infrastructure on AWS using Infrastructure as Code (Terraform and Ansible).

- Become familiar with GitLab and GitLab projects
- Configure a GitLab Runner for your project
- Configure the Runner to build your project
- Become familiar with HashiCorp Terraform and good code structure
- Use Ansible to configure the Wordpress application
- Use Terraform / Ansible to build a highly available deployment of Wordpress

## Outline

- [Lab 0 Lab setup and prerequisites](docs/lab_00_prerequisites.md)

  In this lab you will create an environment with the appropriate tooling and connectivity to proceed with the following labs. You will install tools like Terraform and download a Git repository for deploying a GitLab CI Runner.

- [Lab 1 Your first GitLab repository](docs/lab_01_create_git_repository.md)

  In this lab you will create a free, personal GitLab account, create a GitLab project, and create a CI Runner to execute builds of your project.

- [Lab 2 Your first Terraform project](docs/lab_02_create_base_project.md)

  In this lab you will create begin to check in files to your GitLab project and create a GitLab-CI YAML file. You will provide initial Terraform code to create supporting infrastructure such as an RDS database and an ElastiCache cluster. You will also create a `.gitlab-ci.yml` file to tell the GitLab Runner how to build your project.

- [Lab 3 Deploy Wordpress](docs/lab_03_deploy_wordpress.md)

  In this lab you will deploy the Wordpress application servers that will use the infrastructure created in the previous lab to host a Wordpress website.

---

Let's [get started](docs/lab_00_prerequisites.md).
