resource "random_id" "stack_id" {
  byte_length = 8
}

module "alb_sg" {
  source = "terraform-aws-modules/security-group/aws//modules/http-80"

  name        = "wp-alb-sg-${random_id.stack_id.hex}"
  description = "Security group for Wordpress ALB with HTTP ports open to WWW"
  vpc_id      = local.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
}

module "wp_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "wordpress-sg-${random_id.stack_id.hex}"
  description = "Security group for Wordpress servers"
  vpc_id      = local.vpc_id

  ingress_with_source_security_group_id = [
    {
      rule                     = "http-80-tcp"
      source_security_group_id = module.alb_sg.this_security_group_id
    }
  ]

  egress_rules = ["all-all"]
}
